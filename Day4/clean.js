const fs = require("fs");
const lines = fs
  .readFileSync("puzzle.txt", "utf-8")
  .replace(/\r/g, "")
  .trim()
  .split("\n");

function part1() {
  let count = 0;
  const intervals = lines.map((interval) =>
    interval.split(",").map((x) => x.split("-").map((x) => +x))
  );
  //check if interval2 is inside interval1
  for (const interval of intervals) {
    const [a, b] = interval;

    if (a[0] >= b[0] && a[1] <= b[1]) {
      count++;
    } else if (b[0] >= a[0] && b[1] <= a[1]) {
      count++;
    }
  }
  return count;
}

function part2() {
  let count = 0;
  const intervals = lines.map((interval) =>
    interval.split(",").map((x) => x.split("-").map((x) => +x))
  );
  //check if interval2 is inside interval1
  for (const interval of intervals) {
    const [a, b] = interval;

    if (a[0] >= b[0] && a[0] <= b[1]) {
      count++;
    } else if (b[0] >= a[0] && b[0] <= a[1]) {
      count++;
    }
  }
  return count;
}
console.log(part1());
console.log(part2());
part1();
