const fs = require("fs");
const lines = fs
  .readFileSync("puzzle.txt", "utf-8")
  .replace(/\r/g, "")
  .trim() // Remove strating & ending whitespaces
  .split("\n");

function letterToPriority(letter) {
  if (/[a-z]/.test(letter)) {
    //lowercase
    return letter.charCodeAt(0) - 96;
  } else {
    //uperCase

    return letter.charCodeAt(0) - 38;
  }
}

function part1() {
  const priority = lines.map((line) => {
    const compart1 = [...line.slice(0, line.length / 2)];
    const compart2 = [...line.slice(line.length / 2)];

    //find common element
    let compart1Set = new Set(compart1);
    let common = compart2.filter((x) => compart1Set.has(x));
    let singleCommon = [...new Set(common)];

    return letterToPriority(singleCommon[0]);
  });
  console.log("priority : " + priority.reduce((a, b) => a + b, 0));
}

//const elf2 = [...line.slice(line.length / 2)];

function part2() {
  //Divide input in group of 3 ruckSacks
  var sum = 0;
  for (i = 0; i < lines.length; i += 3) {
    const ruckSacks = [[...lines[i]], [...lines[i + 1]], [...lines[i + 2]]];

    const ruckSacksString = ruckSacks.toString();

    //find common item between the 3 ruckSacks
    let set = new Set(ruckSacks[0]);
    let common = ruckSacks[1].filter((x) => set.has(x));

    set = new Set(common);
    common = ruckSacks[2].filter((x) => set.has(x));
    const singleCommon = [...new Set(common)];

    sum += letterToPriority(singleCommon[0]);
  }
  console.log(sum);
}

//part1();
part2();
