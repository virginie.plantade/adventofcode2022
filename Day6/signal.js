const fs = require("fs");
const input = fs.readFileSync("puzzle.txt", "utf-8").replace(/\r/g, "").trim();

function isUniq(arr) {
  return new Set(arr).size === arr.length;
}

function part1() {
  let signalPart = [];
  for (i = 0; i < input.length; i++) {
    signalPart.push(input[i]);
    //only 4 letter
    if (signalPart.length > 4) {
      signalPart.shift();
    }
    if (signalPart.length === 4 && isUniq(signalPart)) {
      console.log(i + 1);
      break;
    }
  }
}
function part2() {
  let signalPart = [];
  for (i = 0; i < input.length; i++) {
    signalPart.push(input[i]);
    //only 4 letter
    if (signalPart.length > 14) {
      signalPart.shift();
    }
    if (signalPart.length === 14 && isUniq(signalPart)) {
      console.log(i + 1);
      break;
    }
  }
}
// part1();
part2();
