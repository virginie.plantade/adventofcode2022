const fs = require("fs");
const input = fs.readFileSync("puzzle.txt", "utf-8");
const lines = input.split(/\r?\n/);

const valuesOfX = [1];

function countCycles() {
  let x = 1;
  let cycle = 1;
  let total = 0;

  for (const line of lines) {
    const [command, arg] = line.split(" ");

    if (cycle % 40 == 20) {
      total += cycle * x;
    }
    cycle += 1;

    if (command === "addx") {
      if (cycle % 40 == 20) {
        total += cycle * x;
      }

      cycle += 1;
      x += Number(arg);
    }
  }

  return total;
}

module.exports = countCycles();
let values = countCycles(lines);
console.log(values);
