const fs = require("fs");
const parseLines = (filename, trim) => {
  const contents = fs.readFileSync(filename).toString().split("\n");

  if (trim) return contents.map((x) => x.trim());
  return contents;
};
const lines = parseLines("puzzle.txt", true).join(";").split("$ ");

const tree = {
  "/": {},
};

const node = [tree["/"]];
//remove the 2 first line of input
for (let i = 2; i < lines.length; i++) {
  //cd
  if (lines[i].startsWith("cd")) {
    const [, next] = lines[i].replace(";", " ").split(" ");
    //..
    if (next === "..")
      node.shift(); //remove and return the first element of an array.
    else node.unshift(node[0][next]);
  } else {
    const outputs = lines[i]
      .slice(3, lines[i].length)
      .split(";")
      .filter((x) => x !== "");

    //dir
    outputs.forEach((o) => {
      if (o.includes("dir")) {
        const [, dir] = o.split(" ");
        node[0][dir] = {};
      } else {
        const [size, name] = o.split(" ");
        node[0][name] = +size;
      }
    });
  }
}

let root = tree["/"];

const iterateSum = (obj) => {
  let res = 0;
  Object.values(obj).forEach((val) => {
    if (typeof val === "object") res += iterateSum(val);
    else res += val;
  });
  return res;
};

const totalSize = iterateSum(root);
let dirSum = 0;
let dirToDeleteSize = totalSize;

const iterate = (obj) => {
  Object.values(obj).forEach((val) => {
    if (typeof val === "object") {
      const size = iterateSum(val);
      if (size < 100_000) dirSum += size;
      if (
        70_000_000 - totalSize + size >= 30_000_000 &&
        size <= dirToDeleteSize
      )
        dirToDeleteSize = size;
      iterate(val);
    }
  });
};

iterate(root);

// Part 1
console.log(dirSum);

//Part 2
console.log(dirToDeleteSize);
