const fs = require("fs");
const input = fs.readFileSync("puzzle.txt", "utf-8").split(/\r?\n/);

function part1(len = 2) {
  const instructions = input.map((line) => line.split(" "));
  //rope length=2 : Head & Tail
  const knots = new Array(len).fill().map(() => ({ x: 0, y: 0 }));
  const visited = new Set([0, 0]);
  for (const [dir, steps] of instructions) {
    for (let i = 0; i < steps; i++) {
      if (dir === "R") knots[0].x++;
      if (dir === "L") knots[0].x--;
      if (dir === "U") knots[0].y--;
      if (dir === "D") knots[0].y++;
      for (let j = 1; j < knots.length; j++) {
        const [head, tail] = [knots[j - 1], knots[j]];
        if (
          Math.abs(head.x - tail.x) === 2 ||
          Math.abs(head.y - tail.y) === 2
        ) {
          tail.x =
            head.x === tail.x
              ? tail.x
              : head.x > tail.x
              ? tail.x + 1
              : tail.x - 1;
          tail.y =
            head.y === tail.y
              ? tail.y
              : head.y > tail.y
              ? tail.y + 1
              : tail.y - 1;
        }
      }

      visited.add(`${knots[len - 1].x},${knots[len - 1].y}`);
    }
  }
  return visited.size - 1;
}

part1();

function part2() {
  return part1((len = 10));
}

console.log(part1());
console.log(part2());
