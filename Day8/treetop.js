const fs = require("fs");
const matrix = fs
  .readFileSync("puzzle.txt", "utf-8")
  .split(/\r?\n/)
  .map((row) => row.split(""));

const [numberOfRows, numberOfCols] = [matrix.length, matrix[0].length];
console.log({ numberOfRows, numberOfCols });

let res = 0;

//Tree on edge are visible
const isEdge = (row, col) =>
  row === 0 ||
  col === 0 ||
  row === numberOfRows - 1 ||
  col === numberOfCols - 1;

const isVisible = (row, col) => {
  if (isEdge(row, col)) return true;

  //check if all the values in the row and col are less than the value of the cell
  const isValid = (cellValue) => cellValue < matrix[row][col];

  //find col and row value
  const [rowValues, colValues] = [
    matrix[row],
    Array.from({ length: numberOfRows }, (_, i) => matrix[i][col]),
  ];

  return [
    rowValues.slice(0, col).every(isValid),
    rowValues.slice(col + 1).every(isValid),
    colValues.slice(0, row).every(isValid),
    colValues.slice(row + 1).every(isValid),
  ].some(Boolean);
};

const getScenicScore = (row, col) => {
  const [rowValues, colValues] = [
    matrix[row],
    Array.from({ length: numberOfRows }, (_, i) => matrix[i][col]),
  ];

  const treeScore = (cellValue) => {
    if (isEdge(row, col)) return 0;
    for (const [i, x] of cellValue.entries()) {
      if (x >= matrix[row][col] || i === cellValue.length - 1) {
        return i + 1;
      }
    }
  };
  return [
    treeScore(rowValues.slice(0, col).reverse()),
    treeScore(rowValues.slice(col + 1)),
    treeScore(colValues.slice(0, row).reverse()),
    treeScore(colValues.slice(row + 1)),
  ].reduce((acc, score) => acc * score, 1);
};

//activeCell is matrix[row][col]
let allTreesScore = [];
for (let row = 0; row < numberOfRows; row++) {
  for (let col = 0; col < numberOfCols; col++) {
    //check if visible

    res += isVisible(row, col);

    allTreesScore.push(getScenicScore(row, col));
  }
}

//part1
console.log(res);

//part2
sort = allTreesScore.sort((a, b) => a - b).reverse();

console.log(sort[0]);
