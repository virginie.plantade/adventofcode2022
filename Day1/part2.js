const fs = require("fs");
let input = fs.readFileSync("puzzle.txt", "utf-8");

let largest = [];
let current = 0;

function sortArray(array) {
  array.sort(function (a, b) {
    return a - b;
  });
}

function checkAmountCalories() {
  input.split(/\r?\n/).forEach((line) => {
    if (line == "") {
      largest.push(current);
      current = 0;
    } else {
      current += Number(line);
    }
  });
  //Largest sort
  sortArray(largest);
  //I only need the 3 first
  largest = largest.slice(largest.length - 3);
  // Sum Of Calories carried by these 3 elves
  let sum = largest.reduce((acc, val) => {
    return acc + val;
  }, 0);
  console.log("Answer:", sum);
}
checkAmountCalories();
