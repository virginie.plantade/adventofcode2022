const fs = require("fs");
const lines = fs
  .readFileSync("puzzle.txt", "utf-8")
  .replace(/\r/g, "") //remove all \r characters
  .trim() // Remove strating & ending whitespaces
  .split("\n") //Split on new line
  .map((line) => line.split(" ")); //Parse each line into a number

const moves = {
  rock: 1,
  paper: 2,
  scissors: 3,
};
const mapInput = {
  A: moves.rock,
  B: moves.paper,
  C: moves.scissors,
  X: moves.rock,
  Y: moves.paper,
  Z: moves.scissors,
};

function score(opponent, me) {
  if (opponent === me) {
    return me + 3;
  }
  if (
    (opponent === moves.rock && me === moves.paper) ||
    (opponent === moves.paper && me === moves.scissors) ||
    (opponent === moves.scissors && me === moves.rock)
  ) {
    //I win
    return me + 6;
  }
  // I lost
  return me;
}

function part1() {
  const outcomes = lines.map((line) => {
    const opponent = mapInput[line[0]];
    const me = mapInput[line[1]];
    return score(opponent, me);
  });
  console.log(outcomes.reduce((a, b) => a + b, 0));
}

const solutions = {
  A: {
    X: moves.scissors, //loose
    Y: moves.rock, //draw
    Z: moves.paper, //win
  },
  B: {
    X: moves.rock, //loose
    Y: moves.paper, //draw
    Z: moves.scissors, //win
  },
  C: {
    X: moves.paper, //loose
    Y: moves.scissors, //draw
    Z: moves.rock, //win
  },
};

function part2() {
  const outcomes = lines.map((line) => {
    const opponent = mapInput[line[0]];

    //Guess my move from guide
    const me = solutions[line[0]][line[1]];
    return score(opponent, me);
  });
  console.log(outcomes.reduce((a, b) => a + b, 0));
}
part1();
part2();
