const fs = require("fs");
const turns = fs.readFileSync("puzzle.txt", "utf-8").toString();
let turnSplit = turns.split(/\r\n/);

function part1() {
  let score = 0;
  let left = "";
  let right = "";

  turnSplit.forEach((game) => {
    [left, right] = game.split(" ");
    switch (right) {
      case "X":
        score += 1;
        break;
      case "Y":
        score += 2;
        break;
      case "Z":
        score += 3;
        break;
    }

    if (
      (right === "X" && left === "C") ||
      (right === "Y" && left === "A") ||
      (right === "Z" && left === "B")
    ) {
      score += 6;
    } else if (
      (right === "X" && left === "A") ||
      (right === "Y" && left === "B") ||
      (right === "Z" && left === "C")
    ) {
      score += 3;
    }
  });
  return score;
}
 let score2=0;
 let Z =6;
 let Y =3;
 let X = 0;

 let A = 1;
 let B = 2;
 let C = 3;

// function part2() {
//   let score = 0;
//   let left = "";
//   let right = "";

//   turnSplit.forEach((game) => {
//     [left, right] = game.split(" ");
//     switch (right) {
//       case "X":
//         score += 0;
//         break;
//       case "Y":
//         score += 3;
//         break;
//       case "Z":
//         score += 6;
//         break;
//     }
//   });

  
  return score;
}
console.log(part1());
console.log(part2());
