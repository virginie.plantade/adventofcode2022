const fs = require("fs");
const lines = fs.readFileSync("puzzle.txt", "utf-8").replace(/\r/g, "");

let [rawStacks, instructions] = lines.split("\n\n").map((x) => x.split("\n"));

let parsedStacks = rawStacks.map((line) =>
  [...line].filter((value, index) => index % 4 === 1)
);

let cratesId = parsedStacks.pop();

const stacks = {};

for (const line of parsedStacks) {
  for (let i = 0; i < line.length; i++) {
    //if line[i] in not a space
    if (line[i] !== " ") {
      //add line[i] to the stack crateId[i]
      //if stacks[crateId] doesn't exist we need to create one
      if (!stacks[cratesId[i]]) {
        //we need to create one
        stacks[cratesId[i]] = [];
      }
      stacks[cratesId[i]].push(line[i]);
    }
  }
}

const moves = [];
for (const move of instructions) {
  const match = /move (\d+) from (\d+) to (\d+)/g.exec(move);

  moves.push({
    count: parseInt(match[1]),
    from: parseInt(match[2]),
    to: parseInt(match[3]),
  });
}

//part1

function CrateMover9000(stacks, move) {
  for (i = 0; i < move.count; i++) {
    let selected = stacks[move.from].shift();
    stacks[move.to].unshift(selected);
  }
}
function part1() {
  const localStacks = JSON.parse(JSON.stringify(stacks));
  for (const move of moves) {
    CrateMover9000(localStacks, move);
    // for (i = 0; i < cratesId.length; i++) {
    //   console.log(stacks[cratesId[i]][0]);
    // }
  }
  console.log(
    cratesId
      .map((value) => {
        const firstStack = localStacks[value];
        return firstStack[0];
      })
      .join("")
  );
}

//part2() {

function CrateMover9001(stacks, move) {
  const selected = stacks[move.from].splice(0, move.count);
  stacks[move.to].splice(0, 0, selected);
}

function part2() {
  const localStacks = JSON.parse(JSON.stringify(stacks));
  for (const move of moves) {
    const selected = localStacks[move.from].splice(0, move.count);

    localStacks[move.to] = selected.concat(localStacks[move.to]);
  }

  console.log(
    cratesId
      .map((value) => {
        const firstStack = localStacks[value];
        return firstStack[0];
      })
      .join("")
  );
}

// part1();
part2();
